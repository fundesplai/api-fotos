const express = require('express');
const router = express.Router();


const modelo = require('../models/index.js');
const multer = require('multer');

// referencia:
//  https://programmingwithmosh.com/javascript/react-file-upload-proper-server-side-nodejs-easy/

//multer es un plugin que facilita la lectura de archivos procedentes de forms
//aquí se inicializa, indicando que la carpeta es 'uploads'
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads')
    },
    filename: function (req, file, cb) {
        //definimos que los nombres de foto tendrán como prefijo el timestamp actual
        cb(null, Date.now() + '-' + file.originalname)
    }
})
const upload = multer({ storage: storage }).single('file');


//devuelve todas las fotos
router.get('/', (req, res, next) => {
    const token = req.query.token;
    if (!token) {
        res.json({ok: false, error: "Falta token!"})
    };
    modelo.Token.findOne({where: {token}})
    .then((token) =>{
        //mirar caducitat...
        if (!token) {
            res.json({ok: false, error: "token invalid"});
            return;
        } 
    })
    .then(() => modelo.Foto.findAll())
        .then(lista => res.json({ok: true, data: lista}))
        .catch(err => res.json({ ok: false, error: err }));
});

// nueva foto!
// en el body llega la foto (archivo), el título y el comentario
// la puntuación se inicializa a 0
router.post('/', (req, res, next) => {
    upload(req, res, function (err) {
        if (err instanceof multer.MulterError) {
            return res.status(500).json(err)
        } else if (err) {
            return res.status(500).json(err)
        }
        const nuevaFoto = {
            titulo: req.body.titulo,
            comentario: req.body.comentario,
            urlfoto: req.file.filename,
            likes: 0
        }
        modelo.Foto.create(nuevaFoto)
        .then(item => res.json({ ok: true, data: item }))
        .catch(err => res.json({ ok: false, error: err }));
    })
});


router.get('/:id', (req, res, next) => {
    let idfoto = req.params.id;
    // modelo.Foto.findById(idfoto)
    modelo.Foto.findOne({ where: { id: idfoto } })
        .then(item => res.json(item))
        .catch(err => res.json({ ok: false, error: err }));
});

// like, añade un like a la foto
router.get('/like/:id', (req, res, next) => {
    const idfoto = req.params.id;
    modelo.Foto.findOne({ where: { id: idfoto } })
        .then(item => item.update({likes: item.likes+1 }))
        .then(item => res.json(item))
        .catch(err => res.json({ ok: false, error: err }));
});


router.delete('/:id', (req, res, next) => {
    let idfoto = req.params.id;
    modelo.Foto.destroy({ where: { id: idfoto } })
        .then(item => res.json(item))
        .catch(err => res.json({ ok: false, error: err }));
});


module.exports = router;