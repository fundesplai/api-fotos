
'use strict';

module.exports = (sequelize, DataTypes) => {
  const Token = sequelize.define('Token', {
    token: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    idusuari: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    nomusuari: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    minuts: {
      type: DataTypes.INTEGER,
      defaultValue: 120,
      validate: { min: 5, max: 24*60 }
    },
    
  }, { tableName: 'tokens'});
  
  return Token;
};
