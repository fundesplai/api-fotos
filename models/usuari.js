
'use strict';

//definición de modelo SEQUELIZE


module.exports = (sequelize, DataTypes) => {
  const Usuari = sequelize.define('Usuari', {
    // id (como primary key y autoinc), createdAt y updatedAt son campos que sequelize ya incluye por defecto
    nom: {
      type: DataTypes.STRING,
      unique: true,
      allowNull: false,
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    //...
    
  }, { tableName: 'usuaris'});
  
  return Usuari;
};
