'use strict';

module.exports = (sequelize, DataTypes) => {
 
  const Foto = sequelize.define('Foto', {
    titulo: DataTypes.STRING,
    comentario: DataTypes.STRING,
    urlfoto: DataTypes.STRING,
    likes: DataTypes.INTEGER,
  }, { tableName: 'fotos', timestamps: false});
  
  return Foto;

  
};


