const express = require('express');
const cors = require('cors');

const fotoRouter = require('./routes/foto-controller');
var usuariRouter = require('./routes/usuari-controller');

const app = express();
app.use(cors());

app.use(express.json());

app.use("/img", express.static('uploads'));
app.use("/", express.static('public'));
app.use('/fotos', fotoRouter);
app.use('/usuaris', usuariRouter);

const port = 3003;
app.listen(port, ()=>console.log("estamos en http://localhost:"+port));
